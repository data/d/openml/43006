# OpenML dataset: cnn-stock-pred-sp

https://www.openml.org/d/43006

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

This datasets covers features from various categories of technical indicators, futures contracts, price of commodities, important indices of markets around the world, price of major companies in the U.S. market, and treasury bill rates. Sources and thorough description of features have been mentioned in the paper of 'CNNpred: CNN-based stock market prediction using a diverse set of variables.This dataset only contains information from the S&P.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43006) of an [OpenML dataset](https://www.openml.org/d/43006). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43006/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43006/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43006/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

